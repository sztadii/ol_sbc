var header;

header = new function () {

  //catch DOM
  var $el;
  var $flex;
  var $documentTop;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).scroll(function () {
    setTimeout(function () {
      $documentTop = $(document).scrollTop();

      if($documentTop > 150) {
        $el.addClass('-scrolled');
      } else {
        $el.removeClass('-scrolled');
      }
    });
  });

  //private functions
  var init = function () {
    $el = $(".header");
    $flex = $el.find('.header__flex');

    makeSpaceAfter();
  };

  var makeSpaceAfter = function () {
    $el.after('<div class="header__afterSpace"></div>');
  };
};