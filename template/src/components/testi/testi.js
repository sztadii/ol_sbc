var testi;

testi = new function () {

  //private vars
  var breakpoint = 1024;

  //catch DOM
  var $el;
  var $slider;
  var $arrows;

  //bind events
  $(document).ready(function () {
    init();
  });

  //private functions
  var init = function () {
    $el = $(".testi");
    $slider = $el.find('.testi__slider');
    $arrows = $el.find('.testi__arrows');

    $el.imagesLoaded({background: true}).always(function () {
      $slider.slick({
        arrows: true,
        appendArrows: $arrows,
        nextArrow: '<div class="testi__arrowBox -next"><img class="testi__arrow" src="resources/img/testi__arrow-next.svg" alt="arrow"></div>',
        prevArrow: '<div class="testi__arrowBox -prev"><img class="testi__arrow" src="resources/img/testi__arrow-prev.svg" alt="arrow"></div>',
        dots: false,
        fade: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        autoplay: true,
        vertical: false,
        cssEase: 'linear',
        draggable: false,
        pauseOnHover: true,
        pauseOnFocus: false,
        responsive: [
          {
            breakpoint: breakpoint,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    });
  };
};